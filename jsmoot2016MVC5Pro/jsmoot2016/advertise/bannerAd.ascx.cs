﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace jsmoot2011.advertise
{
    public partial class bannerAd : System.Web.UI.UserControl
    {
        private string BannerWidth = "460";
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public string SetWidth
        {
            get { return BannerWidth; }
            set
            {
                BannerWidth = value;
                if (BannerWidth == "100%")
                {
                    AdRotator2.Width = Unit.Percentage(100);
                }
                else
                    AdRotator2.Width = Unit.Pixel(Convert.ToInt32(BannerWidth));
            }
        }
    }
}
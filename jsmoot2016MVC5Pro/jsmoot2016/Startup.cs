﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(jsmoot2016.Startup))]
namespace jsmoot2016
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
